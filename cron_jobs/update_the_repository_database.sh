#!/bin/sh
_http_directory=/srv/http/hyperweb_gnu-plus-linux-libre
_repository_source=https://repo.hyperbola.info:50011/gnu-plus-linux-libre/testing
_arches=(i686 x86_64) # ppc64le riscv64
_multiarches=(x86_64) # riscv64
_whoami=$(whoami)
if [ $_whoami = http ]; then
  cd $_http_directory
  if [ ! -f repository_databases.lock ]; then
    touch repository_databases.lock
    if [ ! -d repository_databases/ ]; then
      mkdir repository_databases/
    fi
    for arch in ${_arches[@]}; do
      if [ ! -d repository_databases/$arch ]; then
        mkdir repository_databases/$arch
      fi
      if [ ! -d repository_databases/$arch/dbdir ]; then
        mkdir repository_databases/$arch/dbdir
      fi
      if [ ! -d repository_databases/$arch/filesdir ]; then
        mkdir repository_databases/$arch/filesdir
      fi
    done
    for arch in ${_arches[@]}; do
      curl $_repository_source/core/os/$arch/core.db.tar.lz > repository_databases/$arch/core.db.tar.lz
      bsdtar -xpf repository_databases/$arch/core.db.tar.lz -C repository_databases/$arch/dbdir
      cd repository_databases/$arch/dbdir
      bsdtar -a -cpf $_http_directory/repository_databases/$arch/core.db.tar.gz $_http_directory/repository_databases/$arch/dbdir/*
      rm -r $_http_directory/repository_databases/$arch/dbdir/*
      cd $_http_directory
      ./manage.py reporead $arch repository_databases/$arch/core.db.tar.gz

      curl $_repository_source/core/os/$arch/core.files.tar.lz > repository_databases/$arch/core.files.tar.lz
      bsdtar -xpf repository_databases/$arch/core.files.tar.lz -C repository_databases/$arch/filesdir
      cd repository_databases/$arch/filesdir
      bsdtar -a -cpf $_http_directory/repository_databases/$arch/core.files.tar.gz $_http_directory/repository_databases/$arch/filesdir/*
      rm -r $_http_directory/repository_databases/$arch/filesdir/*
      cd $_http_directory
      ./manage.py reporead $arch repository_databases/$arch/core.files.tar.gz

      curl $_repository_source/extra/os/$arch/extra.db.tar.lz > repository_databases/$arch/extra.db.tar.lz
      bsdtar -xpf repository_databases/$arch/extra.db.tar.lz -C repository_databases/$arch/dbdir
      cd repository_databases/$arch/dbdir
      bsdtar -a -cpf $_http_directory/repository_databases/$arch/extra.db.tar.gz $_http_directory/repository_databases/$arch/dbdir/*
      rm -r $_http_directory/repository_databases/$arch/dbdir/*
      cd $_http_directory
      ./manage.py reporead $arch repository_databases/$arch/extra.db.tar.gz

      curl $_repository_source/extra/os/$arch/extra.files.tar.lz > repository_databases/$arch/extra.files.tar.lz
      bsdtar -xpf repository_databases/$arch/extra.files.tar.lz -C repository_databases/$arch/filesdir
      cd repository_databases/$arch/filesdir
      bsdtar -a -cpf $_http_directory/repository_databases/$arch/extra.files.tar.gz $_http_directory/repository_databases/$arch/filesdir/*
      rm -r $_http_directory/repository_databases/$arch/filesdir/*
      cd $_http_directory
      ./manage.py reporead $arch repository_databases/$arch/extra.files.tar.gz
    done
    rm repository_databases.lock
  else
    echo 'please wait to terminate this execution'
  fi
else
  echo 'please you need to run as http user'
fi
