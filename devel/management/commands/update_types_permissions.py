from django.core.management.base import BaseCommand
from django.apps import apps
from django.contrib.auth.management import create_permissions
from django.contrib.contenttypes.management import update_contenttypes


class Command(BaseCommand):
    args = '<app app ...>'
    help = 'reloads permissions for specified apps, or all apps if no args are specified'

    def handle(self, *args, **options):
        if not args:
            app_configs = apps.get_app_configs()
        else:
            app_configs = []
            for arg in args:
                apps.append(apps.get_app_config(arg))

        for app_config in app_configs:
            update_contenttypes(app_config, options.get('verbosity', 2))
            create_permissions(app_config, options.get('verbosity', 22))

# vim: set ts=4 sw=4 et:
