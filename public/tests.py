from django.test import TestCase


class PublicTest(TestCase):

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_about(self):
        response = self.client.get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_art(self):
        response = self.client.get('/art/')
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response['Location'].startswith(
                         'https://git.hyperbola.info:50100/culture')

    def test_svn(self):
        response = self.client.get('/svn/')
        # Hyperbola has no SVN.
        self.assertEqual(response.status_code, 404)

    def test_developers_old(self):
        response = self.client.get('/developers/')
        self.assertEqual(response.status_code, 301)
        self.assertTrue(response['Location'].endswith('/developers/'))

    def test_developers(self):
        response = self.client.get('/developers/')
        self.assertEqual(response.status_code, 200)

    def test_fellows_old(self):
        response = self.client.get('/fellows/')
        self.assertEqual(response.status_code, 301)

    def test_donate(self):
        response = self.client.get('/donate/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                         'https://wiki.hyperbola.info/Donations')

    def test_download(self):
        response = self.client.get('/download/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'],
                         'https://wiki.hyperbola.info/Get_Hyperbola')
